import React, {Component} from "react";

import Task from './Task';

class Overview extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { tasks, deleteTask, editTask } = this.props;
        const handlers = { deleteTask, editTask };

        return (
            <div className="Overview">
                <ul>
                    {
                        tasks.map((value, index) => <li key={value.toString()}> <Task task={value} index={index} handlers={handlers} /> </li>)
                    }
                </ul>
            </div>
        )
    }
}

export default Overview;