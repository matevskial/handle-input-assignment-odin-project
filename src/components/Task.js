import React, { Component } from "react"

class Task extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isBeingEdited: false,
        };

        this.handleClickEdit = this.handleClickEdit.bind(this);
        this.applyEditTask = this.applyEditTask.bind(this);
    }

    handleClickEdit() {
        this.setState({
            isBeingEdited: true,
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.isBeingEdited !== this.state.isBeingEdited
        && this.state.isBeingEdited === true) {
            document.getElementById("task").value = this.props.task;
        }
    }

    applyEditTask() {
        const { index } = this.props;
        const { editTask, } = this.props.handlers;
        const newTaskValue = document.getElementById("task").value;

        editTask(index, newTaskValue);
        this.setState({
            isBeingEdited:false,
        })
    }

    render() {
        const { task, index } = this.props;
        const { deleteTask, } = this.props.handlers;
        const { isBeingEdited } = this.state;
        let displayTask;
        let editOrResubmitButton;

        if(isBeingEdited) {
            displayTask = <input type="text" id="task"/>;
            editOrResubmitButton = <button onClick={this.applyEditTask}>Resubmit</button>
        } else {
            displayTask = task;
            editOrResubmitButton = <button onClick={this.handleClickEdit}>Edit</button>
        }

        let deleteButton = <button onClick={(e) => deleteTask(e, index)}>Delete</button>;
        return (
            <>
                {displayTask}
                {editOrResubmitButton}
                {deleteButton}
            </>
        );
    }
}

export default Task;