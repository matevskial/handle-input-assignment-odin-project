import React, {Component} from "react";

import './App.css';
import Overview from "./components/Overview";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tasks: [],
            currentInputTask: "",
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
        this.editTask = this.editTask.bind(this);
    }

    handleSubmit(e) {
        // we don’t want the default behavior of refreshing the form anytime we submit it
        e.preventDefault();

        let { currentInputTask } = this.state;
        this.setState(function (prevState, props) {
            return {
                tasks: prevState.tasks.concat(currentInputTask)
            }
        });

        e.target.task.value = "";
    }

    handleChange(e) {
        this.setState({
            currentInputTask: e.target.value,
        })
    }

    deleteTask(e, indexOfDeletedTask) {
        this.setState((prevState) => ({
            tasks: prevState.tasks.filter((_, i) => i !== indexOfDeletedTask)
        }))
    }

    editTask(indexOfEditedTask, newTask) {
        this.setState((prevState) => ({
            tasks: prevState.tasks.map((value, index) => index === indexOfEditedTask ? newTask : value),
        }));
    }

    render() {
        const { tasks, currentInputTask, } = this.state;
        return (
            <div className="App">
                <form name="theForm" onSubmit={this.handleSubmit}>
                    <input type="text" name="task" value={currentInputTask} onChange={this.handleChange}/>
                    <button type="submit">Submit</button>
                </form>
                <Overview editTask={this.editTask} deleteTask={this.deleteTask} tasks={tasks} />
            </div>
        );
    }
}

export default App;
